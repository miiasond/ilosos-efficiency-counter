-- import System.Random
-- data N = O | P N deriving(Show)

-- to_i :: N -> N
-- to_i O = 0
-- to_i (P a) = to_i a + 1

-- plus (P a) (P b) = to_i(a) + to_i(b)

-- to_i x
-- x => Past a

-- makeRandArr x = 

data Ilosos = Ilosos(IlososClass) deriving(Show)

data IlososClass = IlososA | IlososB | IlososC deriving(Show)

takeClass :: (Ord a, Fractional a) => a -> Ilosos
takeClass a
  | a <= 3.0  = Ilosos(IlososC)
  | a <= 8.0  = Ilosos(IlososB)

think :: (Integral a, Foldable t) => [t a] -> [Ilosos]
think ilos = map (\i -> takeClass(fromIntegral(sum(i)) / fromIntegral(length(i)))) ilos

-- main :: IO()
-- main = putStrLn $ think

