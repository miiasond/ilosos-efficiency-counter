module Main where

data Ilosos = Ilosos(IlososClass) deriving(Show)

data IlososClass = IlososA | IlososB | IlososC deriving(Show)

takeClass :: (Ord a, Fractional a) => a -> Ilosos
takeClass a
  | a <= 3.0  = Ilosos(IlososC)
  | a <= 8.0  = Ilosos(IlososB)

think :: (Integral a, Foldable t) => [t a] -> [Ilosos]
think ilos = map (\i -> takeClass(fromIntegral(sum(i)) / fromIntegral(length(i)))) ilos

-- change array
-- if you have more then one ilosos you need add ,[]
main :: IO()
main = do
  putStrLn $ think [[1,2,3,4,5,6,7,8,9,10]]
